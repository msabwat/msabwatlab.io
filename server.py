#!/usr/bin/env python

import logging
import asyncio
import websockets
import json
import ssl
import pathlib
import os
import time
from subprocess import Popen, PIPE
import socket

#os.system("/bin/bash --rcfile env/bin/activate")

log = logging.getLogger('websockets')
host = "0.0.0.0" #localhost
port = 443

#print IP address to copy in the webapp
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.connect(("8.8.8.8", 80))
print("Server is running on IP :" + s.getsockname()[0])
s.close()

#Pipe to send coordinates
process = Popen('./moveMouse', stdin=PIPE, bufsize=10, universal_newlines=True)

msg = {}

multip = 1.5

async def getMessage(websocket, pathname):
    while 42:
        msg = await websocket.recv()
        msg = json.loads(msg)
        
        if (msg["x"] > 0):
            if (msg["x"] < 10):
                msg["x"] = 0
            else:
                msg["x"] = (msg["x"] + (msg["x"] % 10) + 10) * multip
        elif msg["x"] < 0:
            if (msg["x"] > -10):
                msg["x"] = 0
            else:
                msg["x"] = (msg["x"] - (msg["x"] % 10) - 10) * multip
        if (msg["y"] > 0):
            if (msg["y"] < 10):
                msg["y"] = 0
            else:
                msg["y"] = (msg["y"] + (msg["y"] % 10) + 10) * multip
        elif (msg["y"] < 0):
            if (msg["y"] > -10):
                msg["y"] = 0
            else:
                msg["y"] = (msg["y"] - (msg["y"] % 10) - 10) * multip

        msg = str(msg["y"]) + " " + str(msg["x"]) + "\n"
        process.stdin.flush()
        process.stdin.write(msg)
    process.stdin.close();
    process.wait();

#TODO: Implement secure websocket

#ssl_context = ssl.SSLContext(ssl.PROTOCOL_TLS_SERVER)
#ssl_context.load_cert_chain('localhost.pem')

start_server = websockets.serve(getMessage, host, port)

log.setLevel(logging.INFO)
log.addHandler(logging.StreamHandler())

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
