#ifndef MAGICPOINTER
# define MAGICPOINTER

#include <linux/uinput.h>
#include <linux/input.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/fcntl.h>
#include <string.h>
#include <errno.h>
#include <libevdev-1.0/libevdev/libevdev.h>
#include <libevdev-1.0/libevdev/libevdev-uinput.h>

#include "utils/libft/libft.h"

void moveMouse(struct libevdev_uinput *magicPointer, int x, int y);

#endif
